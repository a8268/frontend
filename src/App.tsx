import { useEffect, useState } from 'react';
import moment from 'moment-timezone';
import Highcharts from 'highcharts/highstock';
import NoDataToDisplay from 'highcharts/modules/no-data-to-display';
import HighchartsReact from 'highcharts-react-official';
import './App.css';

window.moment = moment;

NoDataToDisplay(Highcharts);

function App() {

  useEffect(() => {

    obtainData("temperature_sensor").then((data:any) => {
      setTemperatureChartOptions(getConfigurationChart('Temperature', data));
    });

    obtainData("humidity_sensor").then((data:any) => {
      setHumidityChartOptions(getConfigurationChart('Humidity', data));
    });

    obtainData("luminosity_sensor").then((data:any) => {
      setLuminosityChartOptions(getConfigurationChart('Luminosity', data));
    });

  }, []);

  const getConfigurationChart = (name: string, data=[]) => {
    return {
      time: {
        useUTC: false,
        timezone: 'Europe/Madrid'
      },
      chart: {
        type: 'spline',
        margin: [ 60, 30, 60, 30]
      },
      title: {
        text: name,
      },
      rangeSelector: {
        buttons: [
          {
            count: 1,
            type: "minute",
            text: "1M"
          },
          {
            count: 5,
            type: "minute",
            text: "5M"
          },
          {
            type: "all",
            text: "All"
          }
        ],
        inputEnabled: false,
        selected: 2
      },
      series: [
        {
          name: name,
          data: data
        }
      ],
      lang: {
        noData: "No data available",
      },
      noData: {
        style: {
          fontWeight: 'bold',
          fontSize: '15px',
          color: '#303030',
        },
      }
    };
  }

  const [temperatureChartOptions, setTemperatureChartOptions] = useState(getConfigurationChart('Temperature'));
  const [humidityChartOptions, setHumidityChartOptions] = useState(getConfigurationChart('Humidity'));
  const [luminosityChartOptions, setLuminosityChartOptions] = useState(getConfigurationChart('Luminosity'));


  const obtainData = async (sensor_name: string) => {
    let base_url = "http://localhost:8080";
    const response = await fetch(base_url + "/sensors/" +sensor_name);
    const json_response = await response.json();
    const data = json_response.measurements.map(function(item: { createdAt: string, value: number }) {
      return [window.moment.utc(item.createdAt).toDate().getTime(), item.value]
    });

    return data;
  }

  return (
    <div className="App">
      <header className="App-header">
        <h3>Agriculture Web App to show the status of the monitoring sensors</h3>
      </header>
      <div className="content">
        <div className="graph-container">
          <HighchartsReact
            highcharts={Highcharts}
            constructorType={'stockChart'}
            options={temperatureChartOptions}
          />
        </div>

        <div className="graph-container">
          <HighchartsReact
            highcharts={Highcharts}
            constructorType={'stockChart'}
            options={humidityChartOptions}
          />
        </div>

        <div className="graph-container">
          <HighchartsReact
            highcharts={Highcharts}
            constructorType={'stockChart'}
            options={luminosityChartOptions}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
